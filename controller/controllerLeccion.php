<?php
///////////////////////////////////////////////////////////////////////
//Componente controller que verifica la opcion seleccionada
//por el usuario, ejecuta el modelo y enruta la navegacion de paginas.
///////////////////////////////////////////////////////////////////////
require_once '../model/LeccionModel.php';
require_once '../model/ActividadModel.php';
session_start();
$leccionModel = new LeccionModel();
$actividadModel = new ActividadModel();
$opcion = $_REQUEST['opcion'];
//limpiamos cualquier mensaje previo:
unset($_SESSION['mensaje']);
switch ($opcion) {
    case "listar":
        //obtenemos la lista de notas:
        $listadolecciones = $leccionModel->getLecciones(true);
        //y los guardamos en sesion:
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;
    case "listar_desc":
        //obtenemos la lista de notas:
        $listadolecciones = $leccionModel->getLecciones(false);
        //y los guardamos en sesion:
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;

    case "crear":
        //navegamos a la pagina de creacion:
        header('Location: ../view/leccion/crear.php');
        break;
    case "guardar":
        //obtenemos los valores ingresados por el usuario en el formulario:
        $nombreleccion = $_REQUEST['nombreleccion'];
        $descripcionleccion = $_REQUEST['descripcionleccion'];
        //creamos una nuevo nota:
        try {
            $leccionModel->crearLeccion($nombreleccion, $descripcionleccion);
        } catch (Exception $e) {
            //colocamos el mensaje de la excepcion en sesion:
            $_SESSION['mensaje'] = $e->getMessage();
        }
        //actualizamos la lista de notas para grabar en sesion:
        $listadolecciones = $leccionModel->getLecciones(true);
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;

    case "actividades":
        //para permitirle actualizar un registro al usuario primero
        //obtenemos los datos completos de esa nota:
        $id = $_REQUEST['id_leccion'];
        $leccion = $leccionModel->getLeccion($id);
        $actividades = $actividadModel->getActividadesByIdLeccion($id);
        //guardamos en sesion las notas para posteriormente visualizarlo
        //en un formulario para permitirle al usuario editar los valores:
        $_SESSION['listadoactividades'] = serialize($actividades);
        $_SESSION['leccion'] = serialize($leccion);
        header('Location: ../view/actividad/index.php');
        break;

        case "actividadesuser":
            //para permitirle actualizar un registro al usuario primero
            //obtenemos los datos completos de esa nota:
            $id = $_REQUEST['id_leccion'];
            $leccion = $leccionModel->getLeccion($id);
            $actividades = $actividadModel->getActividadesByIdLeccion($id);
            //guardamos en sesion las notas para posteriormente visualizarlo
            //en un formulario para permitirle al usuario editar los valores:
            $_SESSION['listadoactividades'] = serialize($actividades);
            $_SESSION['leccion'] = serialize($leccion);
            header('Location: ../view/user/actividad.php');
            break;

    case "cargar":
        //para permitirle actualizar un registro al usuario primero
        //obtenemos los datos completos de esa nota:
        $id = $_REQUEST['id_leccion'];
        $leccion = $leccionModel->getLeccion($id);
        //guardamos en sesion las notas para posteriormente visualizarlo
        //en un formulario para permitirle al usuario editar los valores:
        $_SESSION['leccion'] = $leccion;
        header('Location: ../view/leccion/actualizar.php');
        break;

    case "actualizar":
        //obtenemos los datos modificados por el usuario:
        $id = $_REQUEST['id_leccion'];
        $nombre = $_REQUEST['nombre_leccion'];
        $descripcion = $_REQUEST['descripcion_leccion'];
        //actualizamos los datos del notas:
        $leccionModel->actualizarLeccion($id, $nombre, $descripcion);
        //actualizamos la lista de notas para grabar en sesion:
        $listadolecciones = $leccionModel->getLecciones(true);
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;

    case "eliminar":
        //obtenemos el cedula del registro a eliminar:
        $id = $_REQUEST['id_leccion'];
        //eliminamos la nota:
        $leccionModel->eliminarLeccion($id);
        //actualizamos la lista de notas para grabar en sesion:
        $listadolecciones = $leccionModel->getLecciones(true);
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;

    case "cancelar":
        $listadolecciones = $leccionModel->getLecciones(true);
        $_SESSION['listadolecciones'] = serialize($listadolecciones);
        header('Location: ../view/leccion/index.php');
        break;


    default:
        //si no existe la opcion recibida por el controlador, siempre
        //redirigimos la navegacion a la pagina index:
        header('Location: ../index.php');
}
