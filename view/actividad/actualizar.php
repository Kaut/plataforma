<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Actualizar actividad</title>
        <style>
            *{
                font-family: Arial, Helvetica, sans-serif;
            }
        </style>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body style="background-image:  url('../../imagenes/fondo.jpg');">
<div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
<!--ENCABEZADO-->
<div class="jumbotron" style="background-color: cornflowerblue; padding: 30px;">
            <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA</h1>
            <p class="lead" style="color: white;">Actualizar actividad</p>
            <hr class="my-2">
        </div>
        <!--FIN ENCABEZADO-->
    <?php
    include '../../model/Actividad.php';
    //obtenemos los datos de sesion:
    session_start();
    $actividad = $_SESSION['actividad'];
    ?>
    <form action="../../controller/controllerActividad.php" style="margin-left: 6%;margin-right: 6%;">
        <input type="hidden" value="actualizar" name="opcion">
        <!-- Utilizamos pequeños scripts PHP para obtener los valores de la nota: -->
        <input type="hidden" value="<?php echo $actividad->getIdActividad(); ?>" name="id_actividad" readonly>
        <p style="font-size: 20px;">ID: <b><?php echo $actividad->getIdActividad(); ?></b></p><br>
        <b>Nombre:</b><br><input type="text" name="nombre_actividad" value="<?php echo $actividad->getNombreActividad(); ?>" class="form-control" required><br>
        <b>Tipo:</b><br><input type="text" name="tipo_actividad" value="<?php echo $actividad->getTipoActividad(); ?>" class="form-control" required><br>
        <b>Herramienta:</b><br><input type="text" name="herramienta_actividad" value="<?php echo $actividad->getHerramientaActividad(); ?>" class="form-control" required><br>
        <b>Descripción:</b><br><input type="text" name="descripcion_actividad" value="<?php echo $actividad->getDescripcionActividad(); ?>" class="form-control" required><br>
        <b>Link:</b><br><input type="text" name="link_actividad" value="<?php echo $actividad->getLinkActividad(); ?>" class="form-control" required><br>
        <center>
            <input type="submit" value="Actualizar" class="btn btn-primary" style="width: 300px; margin-bottom: 10px;">
        </center>
    </form>
    <center>
        <form action="../../controller/controllerActividad.php">
        <input type="hidden" value="cancelar" name="opcion">
        <input type="submit" value="Cancelar" class="btn btn-primary" style="width: 300px;">
        </form<br>
    </center><br>
    <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div>
</div>
</body>

</html>