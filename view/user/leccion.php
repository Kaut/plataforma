<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD de Lecciones</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <script type="text/javascript"> function confirm_click() { 
        return confirm("¿Estas seguro de eliminar esta lección?"); 
        } 
        </script> 
</head>

<body style="background-image:  url('../../imagenes/fondo.jpg');">
    <!--BLOQUE PRINCIPAL-->
    <div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
        <!--ENCABEZADO-->
        <div class="jumbotron" style="background-color: cornflowerblue; padding: 30px;">
            <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA</h1>
            <p class="lead" style="color: white;">Lecciones</p>
            <hr class="my-2">
        </div>
        <!--FIN ENCABEZADO-->
        <center>
            <table>
                <tr>
                    <td>
                    <a class="btn btn-primary" style="color: white;" href="../../index.php">Inicio <i class="bi bi-box-arrow-in-left"></i></a>
                    </td>
                </tr>
            </table>
            <br>
        </center>
        

                <?php
                session_start();
                include_once '../../model/Leccion.php';
                //verificamos si existe en sesion el listado de notas:
                if (isset($_SESSION['listadolecciones'])) {
                    $listadolecciones = unserialize($_SESSION['listadolecciones']);
                    foreach ($listadolecciones as $lec) {
                    
                       echo "<div class='card mb-2'  style='margin-left: 10px; margin-right: 10px;'>";
                       echo "<div class='card-body'>";
                       echo "<h4 class='card-title'>" . $lec->getNombreLeccion() . "</h4>";
                       echo "<p class='card-text'>" . $lec->getDescripcionLeccion() . "</p>";
                       echo "<a class='btn btn-primary' style='color: white;' href='../../controller/controllerLeccion.php?opcion=actividadesuser&id_leccion=" . $lec->getIdLeccion() . "'>Actividades <i class='bi bi-file-text-fill'></i></a>";
                       echo "</div>";
                       echo "</div>";
                    
                    
                    }
                } else {
                    echo "No se han cargado datos.";
                }
                ?>

        <br>
        <?php
        if (isset($_SESSION['mensaje'])) {
            echo "<br>MENSAJE DEL SISTEMA: <font color='red'>" . $_SESSION['mensaje'] . "</font><br>";
        }
        ?>
        <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div></div>
        <!-- FIN BLOQUE PRINCIPAL-->

</body>

</html>