<?php
///////////////////////////////////////////////////////////////////////
//Componente controller que verifica la opcion seleccionada
//por el usuario, ejecuta el modelo y enruta la navegacion de paginas.
///////////////////////////////////////////////////////////////////////
require_once '../model/ActividadModel.php';
require_once '../model/LeccionModel.php';
session_start();
$actividadModel = new ActividadModel();
$leccionModel = new LeccionModel();
$opcion = $_REQUEST['opcion'];
//limpiamos cualquier mensaje previo:
unset($_SESSION['mensaje']);
switch ($opcion) {
    case "listar":
        //obtenemos la lista de notas:
        $leccion = unserialize($_SESSION['leccion']);
        $listadoactividades = $actividadModel->getActividadesByIdLeccion($leccion->getIdLeccion());
        $_SESSION['listadoactividades'] = serialize($listadoactividades);
        header('Location: ../view/actividad/index.php');
        break;
    case "listar_desc":
        //obtenemos la lista de notas:
        $listadoactividades = $actividadModel->getActividades(false);
        //y los guardamos en sesion:
        $_SESSION['listadoactividades'] = serialize($listadoactividades);
        header('Location: ../view/actividad/index.php');
        break;

    case "crear":
        //navegamos a la pagina de creacion:

        header('Location: ../view/actividad/crear.php');
        break;
    case "guardar":
        //obtenemos los valores ingresados por el usuario en el formulario:
        $idLeccion = $_REQUEST['id_leccion'];
        $_SESSION['id_leccion'] = $idLeccion;
        $nombreact = $_REQUEST['nombreact'];
        $tipoact = $_REQUEST['tipoact'];
        $herramientact = $_REQUEST['herramientaact'];
        $descripcionact = $_REQUEST['descripcionact'];
        $linkact = $_REQUEST['linkact'];
        //creamos una nuevo nota:
        try {
            $actividadModel->crearActividad($idLeccion, $nombreact, $tipoact, $herramientact, $descripcionact, $linkact);
        } catch (Exception $e) {
            //colocamos el mensaje de la excepcion en sesion:
            $_SESSION['mensaje'] = $e->getMessage();
        }
        //actualizamos la lista de notas para grabar en sesion:
        $idlec = $_REQUEST['id_leccion'];
        $actividades = $actividadModel->getActividadesByIdLeccion($idlec);
        $_SESSION['listadoactividades'] = serialize($actividades);
        header('Location: ../view/actividad/index.php');
        break;

    case "cargar":
        //para permitirle actualizar un registro al usuario primero
        //obtenemos los datos completos de esa nota:
        $id = $_REQUEST['id_actividad'];
        $actividad = $actividadModel->getActividad($id);
        //guardamos en sesion las notas para posteriormente visualizarlo
        //en un formulario para permitirle al usuario editar los valores:
        $_SESSION['actividad'] = $actividad;
        header('Location: ../view/actividad/actualizar.php');
        break;
    case "actualizar":
        //obtenemos los datos modificados por el usuario:
        $id = $_REQUEST['id_actividad'];
        $nombreact = $_REQUEST['nombre_actividad'];
        $tipoact = $_REQUEST['tipo_actividad'];
        $herramientact = $_REQUEST['herramienta_actividad'];
        $descripcionact = $_REQUEST['descripcion_actividad'];
        $linkact = $_REQUEST['link_actividad'];
        //actualizamos los datos del notas:
        $actividadModel->actualizarActividad($id, $nombreact, $tipoact, $herramientact, $descripcionact, $linkact);
        //actualizamos la lista de notas para grabar en sesion:
        $leccion = unserialize($_SESSION['leccion']);
        $listadoactividades = $actividadModel->getActividadesByIdLeccion($leccion->getIdLeccion());
        $_SESSION['listadoactividades'] = serialize($listadoactividades);
        header('Location: ../view/actividad/index.php');
        break;

    case "eliminar":
        //obtenemos el cedula del registro a eliminar:
        $id = $_REQUEST['id_actividad'];
        //eliminamos la nota:
        $actividadModel->eliminarActividad($id);
        //actualizamos la lista de notas para grabar en sesion:
        //$listadoactividades = $actividadModel->getActividades(true);
        //$_SESSION['listadoactividades'] = serialize($listadoactividades);
        $leccion = unserialize($_SESSION['leccion']);
        $listadoactividades = $actividadModel->getActividadesByIdLeccion($leccion->getIdLeccion());
        $_SESSION['listadoactividades'] = serialize($listadoactividades);
        header('Location: ../view/actividad/index.php');
        break;

    case "cancelar":
        $leccion = unserialize($_SESSION['leccion']);
        $listadoactividades = $actividadModel->getActividadesByIdLeccion($leccion->getIdLeccion());
        $_SESSION['listadoactividades'] = serialize($listadoactividades);
        header('Location: ../view/actividad/index.php');
        break;

    case "lecciones":
        header('Location: ../view/leccion/index.php');
        break;

        case "leccionesuser":
            header('Location: ../view/user/leccion.php');
            break;

    default:
        //si no existe la opcion recibida por el controlador, siempre
        //redirigimos la navegacion a la pagina index:
        header('Location: ../index.php');
}
