<?php
include_once 'Database.php';
include_once 'Leccion.php';
/**
 * Componente model para el manejo de notas.
 
 * @author UlcuangoK
 */
class LeccionModel
{
    
    /**
     * Obtiene todos los lecciones de la base de datos.
     * @return array
     */
    public function getLecciones($orden)
    {
        //obtenemos la informacion de la bdd:
        $pdo = Database::connect();
        //verificamos el ordenamiento asc o desc:
        if ($orden == true) //asc
            $sql = "select * from leccion order by id_leccion";
        else //desc
            $sql = "select * from leccion order by id_leccion desc";
        $resultado = $pdo->query($sql);
        //transformamos los registros en objetos de tipo notas:
        $listadolecciones = array();
        foreach ($resultado as $res) {
            $leccion = new Leccion();
            $leccion->setIdLeccion($res['id_leccion']);
            $leccion->setNombreLeccion($res['nombre_leccion']);
            $leccion->setDescripcionLeccion($res['descripcion_leccion']);
            array_push($listadolecciones, $leccion);
        }
        Database::disconnect();
        //retornamos el listado resultante:
        return $listadolecciones;
    }
    
    public function crearLeccion($nombre, $descripcion)
    {
        //Preparamos la conexion a la bdd:
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Preparamos la sentencia con parametros:
        $sql = "insert into leccion (nombre_leccion,descripcion_leccion) values(?,?)";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos y pasamos los parametros:

        try {
            $consulta->execute(array($nombre, $descripcion));
        } catch (PDOException $e) {
            Database::disconnect();
            throw new Exception($e->getMessage());
        }
        Database::disconnect();
    }

    public function getLeccion($id)
    {
        //Obtenemos la informacion de la nota especifico:
        $pdo = Database::connect();
        //Utilizamos parametros para la consulta:
        $sql = "select * from leccion where id_leccion=?";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos y pasamos los parametros para la consulta:
        $consulta->execute(array($id));
        //Extraemos el registro especifico:
        $dato = $consulta->fetch(PDO::FETCH_ASSOC);
        //Transformamos el registro obtenido a objeto:
        $leccion = new Leccion();
        $leccion->setIdLeccion($dato['id_leccion']);
        $leccion->setNombreLeccion($dato['nombre_leccion']);
        $leccion->setDescripcionLeccion($dato['descripcion_leccion']);
        Database::disconnect();
        return $leccion;
    }

    public function actualizarLeccion($id, $nombre, $descripcion)
    {
        //Preparamos la conexión a la bdd:
        $pdo = Database::connect();
        $sql = "update leccion set nombre_leccion=?,descripcion_leccion=? where id_leccion=?";
        $consulta = $pdo->prepare($sql);

        //Ejecutamos la sentencia incluyendo a los parametros:
        $consulta->execute(array($nombre, $descripcion, $id));
        Database::disconnect();
    }

    public function eliminarLeccion($id)
    {
        //Preparamos la conexion a la bdd:
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "delete from leccion where id_leccion=?";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos la sentencia incluyendo a los parametros:
        $consulta->execute(array($id));
        Database::disconnect();
    }
    
}
