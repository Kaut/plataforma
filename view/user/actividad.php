<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD de Actividadess</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <script type="text/javascript"> function confirm_click() { 
        return confirm("¿Estas seguro de eliminar esta actividad?"); 
        } 
        </script> 
</head>

<body style="background-image:  url('../../imagenes/fondo.jpg');">
    <!--BLOQUE PRINCIPAL-->
    <div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
        <!--ENCABEZADO-->
        <div class="jumbotron" style="background-color: cornflowerblue; padding: 30px;">
            <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA</h1>
            <p class="lead" style="color: white;">Actividades</p>
            <hr class="my-2">
        </div>
        <!--FIN ENCABEZADO-->

        <center>
            <table>
                <tr>
                    <td>
                        <form action="../../controller/controllerActividad.php">
                            <input type="hidden" value="leccionesuser" name="opcion">
                            <button type="submit" value="Regresar" class="btn btn-primary">
                            Regresar <i class="bi bi-box-arrow-in-left"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </table>
            <br>
        </center>

        <center>
            <table border="1" class="table table-hover table-bordered table-striped table-responsive-md" style="background-color: white;background-color: rgba(255,255,255,0.6);">
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>TIPO</th>
                    <th>HERRAMIENTA</th>
                    <th>DESCRIPCIÓN</th>
                    <th>LINK</th>
                </tr>
                <?php
                session_start();
                include_once '../../model/Actividad.php';
                //verificamos si existe en sesion el listado de notas:
                if (isset($_SESSION['listadoactividades'])) {
                    $listadoactividades = unserialize($_SESSION['listadoactividades']);
                    foreach ($listadoactividades as $act) {
                        echo "<tr>";
                        echo "<td>" . $act->getIdActividad() . "</td>";
                        echo "<td>" . $act->getNombreActividad() . "</td>";
                        echo "<td>" . $act->getTipoActividad() . "</td>";
                        echo "<td>" . $act->getHerramientaActividad() . "</td>";
                        echo "<td>" . $act->getDescripcionActividad() . "</td>";
                        echo "<td><center><a href='" . $act->getLinkActividad() . "' target='_blank'><i class='bi bi-globe2'></i> Ir</a></center></td>";
                        echo "</tr>";
                    }
                } else {
                    echo "No se han cargado datos.";
                }
                ?>
            </table><br>
        </center>
        <br>
        <?php
        if (isset($_SESSION['mensaje'])) {
            echo "<br>MENSAJE DEL SISTEMA: <font color='red'>" . $_SESSION['mensaje'] . "</font><br>";
        }
        ?>
        <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div></div>
        <!-- FIN BLOQUE PRINCIPAL-->

</body>

</html>