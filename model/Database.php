<?php

class Database
{
    private static $dbName = 'dfl6puplsgi4es';
    private static $dbHost = 'ec2-23-23-164-251.compute-1.amazonaws.com';
    private static $dbUsername = 'xzzwfqgepgjmmx';
    private static $dbUserPassword = 'dfc87d93e7195606a166d57dd1358d7172ee9244b9bf988510f309e0d077a7cc';

    private static $conexion = null;
    

    public function __construct()
    {
        exit('No se permite instanciar esta clase.');
    }


    public static function connect()
    {
        // Una sola conexion para toda la aplicacion (singleton):
    
        if (null == self::$conexion) {
            try {
                self::$conexion = new PDO("pgsql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
// "host=ec2-23-23-164-251.compute-1.amazonaws.com port=5432 dbname=dfl6puplsgi4es user=xzzwfqgepgjmmx password=dfc87d93e7195606a166d57dd1358d7172ee9244b9bf988510f309e0d077a7cc");
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$conexion;
    }


    public static function disconnect()
    {
        self::$conexion = null;
    }
}