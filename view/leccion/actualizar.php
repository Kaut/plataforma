<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Actualizar lección</title>
        <style>
            *{
                font-family: Arial, Helvetica, sans-serif;
            }
        </style>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body style="background-image:  url('../../imagenes/fondo.jpg');">
<div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
<!--ENCABEZADO-->
<div class="jumbotron" style="background-color: cornflowerblue; padding: 30px;">
            <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA</h1>
            <p class="lead" style="color: white;">Actualizar lección</p>
            <hr class="my-2">
        </div>
        <!--FIN ENCABEZADO-->
    <?php
    include '../../model/Leccion.php';
    //obtenemos los datos de sesion:
    session_start();
    $leccion = $_SESSION['leccion'];
    ?>
    <form action="../../controller/controllerLeccion.php" style="margin-left: 6%;margin-right: 6%;">
        <input type="hidden" value="actualizar" name="opcion">
        <!-- Utilizamos pequeños scripts PHP para obtener los valores de la nota: -->
        <input type="hidden" value="<?php echo $leccion->getIdLeccion(); ?>" name="id_leccion" readonly>
        <p style="font-size: 20px;">ID: <b><?php echo $leccion->getIdLeccion(); ?></b></p><br>
        <b>Nombre:</b><input type="text" name="nombre_leccion" value="<?php echo $leccion->getNombreLeccion(); ?>" class="form-control" required><br>
        <b>Descripción:</b><input type="text" name="descripcion_leccion" value="<?php echo $leccion->getDescripcionLeccion(); ?>" class="form-control"><br>
        <center>
            <input type="submit" value="Actualizar" class="btn btn-primary" style="width: 300px; margin-bottom: 10px;">
        </center>
    </form>
    <center>
        <form action="../../controller/controllerLeccion.php">
        <input type="hidden" value="cancelar" name="opcion">
        <input type="submit" value="Cancelar" class="btn btn-primary" style="width: 300px;">
        </form<br>
    </center><br>
    <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div>
</div>
</body>

</html>