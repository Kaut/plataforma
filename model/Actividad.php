<?php

/**
 * Entidad Producto. Representa a la tabla actividad en la base de datos.
 *
 * @author UlcuangoK
 */
class Actividad
{
    private $id_actividad;
    private $id_leccion;
    private $nombre_actividad;
    private $tipo_actividad;
    private $herramienta_actividad;
    private $descripcion_actividad;
    private $link_actividad;


    /**
     * Get the value of id_actividad
     */
    public function getIdActividad()
    {
        return $this->id_actividad;
    }

    /**
     * Set the value of id_actividad
     *
     * @return  self
     */
    public function setIdActividad($id_actividad)
    {
        $this->id_actividad = $id_actividad;

        return $this;
    }

    /**
     * Get the value of id_leccion
     */
    public function getIdLeccion()
    {
        return $this->id_leccion;
    }

    /**
     * Set the value of id_leccion
     *
     * @return  self
     */
    public function setIdLeccion($id_leccion)
    {
        $this->id_leccion = $id_leccion;

        return $this;
    }


    /**
     * Get the value of nombre_actividad
     */
    public function getNombreActividad()
    {
        return $this->nombre_actividad;
    }

    /**
     * Set the value of nombre_actividad
     *
     * @return  self
     */
    public function setNombreActividad($nombre_actividad)
    {
        $this->nombre_actividad = $nombre_actividad;

        return $this;
    }



    /**
     * Get the value of tipo_actividad
     */
    public function getTipoActividad()
    {
        return $this->tipo_actividad;
    }

    /**
     * Set the value of tipo_actividad
     *
     * @return  self
     */
    public function setTipoActividad($tipo_actividad)
    {
        $this->tipo_actividad = $tipo_actividad;

        return $this;
    }
     

        /**
     * Get the value of herramienta_actividad
     */
    public function getHerramientaActividad()
    {
        return $this->herramienta_actividad;
    }

    /**
     * Set the value of herramienta_actividad
     *
     * @return  self
     */
    public function setHerramientaActividad($herramienta_actividad)
    {
        $this->herramienta_actividad = $herramienta_actividad;

        return $this;
    }


      /**
     * Get the value of descripcion_actividad
     */
    public function getDescripcionActividad()
    {
        return $this->descripcion_actividad;
    }

    /**
     * Set the value of descripcion_actividad
     *
     * @return  self
     */
    public function setDescripcionActividad($descripcion_actividad)
    {
        $this->descripcion_actividad = $descripcion_actividad;

        return $this;
    }


    /**
     * Get the value of link_actividad
     */
    public function getLinkActividad()
    {
        return $this->link_actividad;
    }

    /**
     * Set the value of link_actividad
     *
     * @return  self
     */
    public function setLinkActividad($link_actividad)
    {
        $this->link_actividad = $link_actividad;

        return $this;
    }
}
