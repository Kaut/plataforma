<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Aprendiendo Kichwa</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <script type="text/javascript">
        function confirm_click() {
            return confirm("¿Estas seguro de eliminar las notas?");
        }
    </script>
</head>

<body style="background-image:  url('imagenes/fondo.jpg');">
    <!--BLOQUE PRINCIPAL-->
    <div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
        <!--ENCABEZADO-->
        <div class="jumbotron" style="background-color: cornflowerblue;">
            <div style="padding: 30px;">
                <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA </h1>
                <hr class="my-2">
            </div>
            <nav class="navbar navbar-expand-lg navbar-light" style="background-color: cornflowerblue; color: white;">
                <a class="navbar-brand" href="#" style="color: white;"></a>
                <div>
                    <a class="nav-link" href="./view/leccion/index.php" style="color: white; border-radius: 2px;"><i class="bi bi-lock-fill"></i> </a>
                </div>
            </nav>


        </div>


        <!--FIN ENCABEZADO-->

        <center>
        <div class="embed-responsive embed-responsive-16by9" style="max-width: 1200px;">
            <iframe class="embed-responsive-item" style="padding: 5%;" src="https://www.youtube.com/embed/JDqobSnX8H8?rel=0" allowfullscreen></iframe>
        </div>
        </center>
        <div class="row">
        <div class="col-md-4" style="padding: 3%;">
            <div class="card mb-2">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                   alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title">¿Qué es "Aprendiendo Kichwa"?</h4>
                <p class="card-text">El potencial de varias herramienteas de aprendizaje online a su dispocición.</p>
                <a class="btn btn-primary" style="color: white;">Ver más</a>
              </div>
            </div>
          </div>
          <div class="col-md-4" style="padding: 3%;">
            <div class="card mb-2">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                   alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title">Lecciones</h4>
                <p class="card-text">Aprenda Kichwa de forma didáctica mediante una variedad de herrmientas online.</p>
                <a class="btn btn-primary" style="color: white;" href="./view/user/leccion.php">Empezar</a>
              </div>
            </div>
          </div>

          

          

          <div class="col-md-4" style="padding: 3%;">
            <div class="card mb-2">
              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                   alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title">Sobre nosostros</h4>
                <p class="card-text">Informete acerca del equipo detras de esta aplicación. </p>
                <a class="btn btn-primary" style="color: white;">Ver</a>
              </div>
            </div>
          </div>
        </div>

        <?php
        if (isset($_SESSION['mensaje'])) {
            echo "<br>MENSAJE DEL SISTEMA: <font color='red'>" . $_SESSION['mensaje'] . "</font><br>";
        }
        ?>
        <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div>
        <!-- FIN BLOQUE PRINCIPAL-->

</body>

</html>