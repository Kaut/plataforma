<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Añadir lección</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body style="background-image:  url('../../imagenes/fondo.jpg');">
    <div style="margin-left: 6%;margin-right: 6%; background-color: rgba(255,255,255,0.8);">
        <!--ENCABEZADO-->
        <div class="jumbotron" style="background-color: cornflowerblue; padding: 30px;">
            <h1 class="display-6" style="color: white;">APRENDIENDO KICHWA</h1>
            <p class="lead" style="color: white;">Añadir lección</p>
            <hr class="my-2">
        </div>
        <!--FIN ENCABEZADO-->
        <div class="form-group" style="margin-left: 5%;margin-right: 5%;">
        <form action="../../controller/controllerLeccion.php">
            <input type="hidden" value="guardar" name="opcion">
            <b>Nombre:</b><br><input type="text" name="nombreleccion" class="form-control" required><br>
            <b>Descripción:</b><br><input type="text" name="descripcionleccion" class="form-control" required><br>
            <center>
                <input type="submit" value="Añadir" class="btn btn-primary" style="width: 300px; margin-bottom: 10px;">
            </center>
        </form>
        <center>
        <form action="../../controller/controllerLeccion.php">
        <input type="hidden" value="cancelar" name="opcion">
        <input type="submit" value="Cancelar" class="btn btn-primary" style="width: 300px;">
        </form<br>
        </center>
        </div>
        <div style="background-color: cornflowerblue; padding: 15px; margin-bottom: 30px;">
            <p style="color: white;text-align: center;">©Digital Mayhem 2021</p>
        </div>
    </div>
</body>

</html>