<?php

/**
 * Entidad Leccion. Representa a la tabla leccion en la base de datos.
 *
 * @author UlcuangoK
 */
class Leccion
{
    private $id_leccion;
    private $nombre_leccion;
    private $descripcion_leccion;


    /**
     * Get the value of id_leccion
     */
    public function getIdLeccion()
    {
        return $this->id_leccion;
    }

    /**
     * Set the value of id_leccion
     *
     * @return  self
     */
    public function setIdLeccion($id_leccion)
    {
        $this->id_leccion = $id_leccion;

        return $this;
    }


     /**
     * Get the value of nombre_leccion
     */
    public function getNombreLeccion()
    {
        return $this->nombre_leccion;
    }

    /**
     * Set the value of nombre_leccion
     *
     * @return  self
     */
    public function setNombreLeccion($nombre_leccion)
    {
        $this->nombre_leccion = $nombre_leccion;

        return $this;
    }



    /**
     * Get the value of descripcion_leccion
     */
    public function getDescripcionLeccion()
    {
        return $this->descripcion_leccion;
    }

    /**
     * Set the value of descripcion_leccion
     *
     * @return  self
     */
    public function setDescripcionLeccion($descripcion_leccion)
    {
        $this->descripcion_leccion = $descripcion_leccion;

        return $this;
    }

}
