<?php
include_once 'Database.php';
include_once 'Actividad.php';
include_once 'Leccion.php';

/**
 * Componente model para el manejo de notas.
 
 * @author UlcuangoK
 */
class ActividadModel
{
    
    /**
     * Obtiene todos los lecciones de la base de datos.
     * @return array
     */
    public function getActividades($orden)
    {
        //obtenemos la informacion de la bdd:
        $pdo = Database::connect();
        //verificamos el ordenamiento asc o desc:
        if ($orden == true) //asc
            $sql = "select * from actividad order by id_actividad";
        else //desc
            $sql = "select * from actividad order by id_actividad desc";
        $resultado = $pdo->query($sql);
        //transformamos los registros en objetos de tipo notas:
        $listadoactividades = array();
        foreach ($resultado as $res) {
            $actividad = new Actividad();
            $actividad->setIdActividad($res['id_actividad']);
            $actividad->setIdLeccion($res['id_leccion']);
            $actividad->setNombreActividad($res['nombre_actividad']);
            $actividad->setTipoActividad($res['tipo_actividad']);
            $actividad->setHerramientaActividad($res['herramienta_actividad']);
            $actividad->setDescripcionActividad($res['descripcion_actividad']);
            $actividad->setLinkActividad($res['link_actividad']);
            array_push($listadoactividades, $actividad);
        }
        Database::disconnect();
        //retornamos el listado resultante:
        return $listadoactividades;
    }

    public function getActividadesByIdLeccion($idLeccion)
    {
        //obtenemos la informacion de la bdd:
        $pdo = Database::connect();
        $sql = "select * from actividad where id_leccion=? order by id_actividad";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos y pasamos los parametros:
        try {
            $consulta->execute(array($idLeccion));
        } catch (PDOException $e) {
            Database::disconnect();
            throw new Exception($e->getMessage());
        }
        //transformamos los registros en objetos de tipo notas:
        $listadoactividades = array();
        foreach ($consulta as $res) {
            $actividad = new Actividad();
            $actividad->setIdActividad($res['id_actividad']);
            $actividad->setIdLeccion($res['id_leccion']);
            $actividad->setNombreActividad($res['nombre_actividad']);
            $actividad->setTipoActividad($res['tipo_actividad']);
            $actividad->setHerramientaActividad($res['herramienta_actividad']);
            $actividad->setDescripcionActividad($res['descripcion_actividad']);
            $actividad->setLinkActividad($res['link_actividad']);
            array_push($listadoactividades, $actividad);
        }
        Database::disconnect();
        //retornamos el listado resultante:
        return $listadoactividades;
    }
    
    public function crearActividad($idLeccion, $nombreact, $tipoact, $herramientact, $descripcionact, $linkact)
    {
        //Preparamos la conexion a la bdd:
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Preparamos la sentencia con parametros:
        $sql = "insert into actividad (id_leccion,nombre_actividad,tipo_actividad,herramienta_actividad,descripcion_actividad,link_actividad) values(?,?,?,?,?,?)";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos y pasamos los parametros:
        try {
            $consulta->execute(array($idLeccion, $nombreact, $tipoact, $herramientact, $descripcionact, $linkact));
        } catch (PDOException $e) {
            Database::disconnect();
            throw new Exception($e->getMessage());
        }
        Database::disconnect();
    }

    public function getActividad($id)
    {
        //Obtenemos la informacion de la nota especifico:
        $pdo = Database::connect();
        //Utilizamos parametros para la consulta:
        $sql = "select * from actividad where id_actividad=?";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos y pasamos los parametros para la consulta:
        $consulta->execute(array($id));
        //Extraemos el registro especifico:
        $dato = $consulta->fetch(PDO::FETCH_ASSOC);
        //Transformamos el registro obtenido a objeto:
        $actividad = new Actividad();
        $actividad->setIdActividad($dato['id_actividad']);
        $actividad->setIdLeccion($dato['id_leccion']);
        $actividad->setNombreActividad($dato['nombre_actividad']);
        $actividad->setTipoActividad($dato['tipo_actividad']);
        $actividad->setHerramientaActividad($dato['herramienta_actividad']);
        $actividad->setDescripcionActividad($dato['descripcion_actividad']);
        $actividad->setLinkActividad($dato['link_actividad']);
        Database::disconnect();
        return $actividad;
    }

    public function actualizarActividad($id, $nombreact, $tipoact, $herramientact, $descripcionact, $linkact)
    {
        //Preparamos la conexión a la bdd:
        $pdo = Database::connect();
        $sql = "update actividad set nombre_actividad=?,tipo_actividad=?,herramienta_actividad=?,descripcion_actividad=?,link_actividad=? where id_actividad=?";
        $consulta = $pdo->prepare($sql);

        //Ejecutamos la sentencia incluyendo a los parametros:
        $consulta->execute(array($nombreact, $tipoact, $herramientact, $descripcionact, $linkact, $id));
        Database::disconnect();
    }

    public function eliminarActividad($id)
    {
        //Preparamos la conexion a la bdd:
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "delete from actividad where id_actividad=?";
        $consulta = $pdo->prepare($sql);
        //Ejecutamos la sentencia incluyendo a los parametros:
        $consulta->execute(array($id));
        Database::disconnect();
    }
    
}
